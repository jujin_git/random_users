import axios from 'axios';
const baseUrl = 'https://randomuser.me/api/?';


 const getUsers =  async (results, page) => {
    let url = `${baseUrl}results=${results}&page=${page}&seed=abc`;
    let users =  await  axios.get(url)
    return users;
}
export default {
    getUsers,
}

