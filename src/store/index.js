import Vue from 'vue'
import Vuex from 'vuex'
import apiClient from "@/helpers/apiClient";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    users: null,
    selectedUser: '',
    selectedNumber: 10,
    page: 1,
  },
  plugins: [createPersistedState()],
  getters: {
    getUsers: (state) => state.users?.data?.results || [],
    getSelectedUser: (state) => state.selectedUser,
    getSelectedNumber: (state) => state.selectedNumber,
    getPage: (state) => state.page
  },
  mutations: {
    setUsers(state, users) {
      state.users = users;
    },
    setSelectedUser(state, user) {
      state.selectedUser = user;
    },
    setSelectedNumber(state, number) {
      state.selectedNumber = number;
    },
    setPage(state, number) {
      state.page = number;
    },
  },
  actions: {
    async setUsers({commit}, results) {
      let a = await apiClient.getUsers(results.selectedNumber, results.selectedPage)
      commit('setUsers', a);
    }
  },
  modules: {
  }
})
